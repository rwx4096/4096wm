const std   = @import("std");
const ascii = std.ascii;
const fmt   = std.fmt;
const fs    = std.fs;
const io    = std.io;
const mem   = std.mem;
const log   = @import("log.zig");
const c     = @cImport({
    @cInclude("xcb/xcb.h");
    // @cInclude("xcb/xcb_keysyms.h");
    @cInclude("X11/keysym.h");
});

pub const super : u16 = c.XCB_MOD_MASK_4;
pub const ctrl  : u16 = c.XCB_MOD_MASK_CONTROL;
pub const alt   : u16 = c.XCB_MOD_MASK_1;
pub const shift : u16 = c.XCB_MOD_MASK_SHIFT;

pub var cursor : []const u8 = "left_ptr";
pub var mode : enum { master, stack } = .master;
pub const Bg = opaque {
    pub var col  :  []const u8 = "Blue";
    pub var path : ?[]const u8 = "wallpaper.bmp";
};
pub const Win = opaque {
    pub const Border = opaque {
        pub const Focus = opaque {
            // pub var size : u32 = 3;
            pub var col  : u32 = 0xFF00FF;
        };
        pub const Unfocus = opaque {
            // pub var size : u32 = 3;
            pub var col  : u32 = 0x00FF00;
        };
        pub var size : u32 = 3;
    };
    pub var gap : u32 = 10;
};
pub const Bar = opaque {
    pub var gap : u32 = 60;
};

pub fn parseAndAplyConf(wm : *@import("4096wm.zig")) void {
    const file = fs.openFileAbsolute(fs.path.join(wm.allocator,
        &[_][]const u8{
            std.os.getenv("HOME") orelse {
                log.err("os.getenv", .{}); // TODO
                return;
            },
            ".config/4096wm.conf",
        }) catch |err| {
            log.err("fs.path.join (error: {s})", .{ @errorName(err) }); // TODO
            return;
        },
        .{ .mode = .read_only }, // TODO: More flags??
    ) catch |err| {
        log.err("fs.openFileAbsolute (error: {s})", .{ @errorName(err) }); // TODO
        return;
    };
    defer file.close();
    const reader = file.reader();
    outer: while (reader.readUntilDelimiterOrEofAlloc(wm.allocator, '\n', 4096 * 4)) |buf| {
        var it = if (buf) |x| mem.tokenizeAny(u8, x, &ascii.whitespace) else break;
        while (it.next()) |tok| {
            if (tok[0] == '#') continue :outer;
            // TODO: Test other hashing algorithms.
            var hash = std.hash.Fnv1a_64.init(); // XxHash64.init();
            hash.update(tok);
            const num = fmt.parseInt(u32, if (it.next()) |x| x else continue :outer, 0) catch continue :outer; // TODO
            switch (hash.final()) {
                0x0A0EF492F61DABEA => Win.Border.size        = num,
                0xF067FA7331E5098D => Win.Border.Focus.col   = num,
                0x282EAAA1722DD89C => Win.Border.Unfocus.col = num,
                0x7EFEECFE263BD8DF => Win.gap                = num,
                0x29C2F562517B4F30 => Bar.gap                = num,
                else               => continue :outer, // TODO
            }
        }
    } else |err| {
        log.err("reader.readUntilDeliminerOrEofAlloc (error: {s})", .{ @errorName(err) }); // TODO
        return;
    }
    // if (wm.addKeybind(super | shift, c.XK_q,      null, null))       return;
    // if (wm.addKeybind(super,         c.XK_f,      wm.full, null))    return;
    // if (wm.addKeybind(super,         c.XK_q,      wm.kill, null))    return;
    // if (wm.addKeybind(super,         c.XK_Return, wm.exec, "xterm")) return;
    // if (wm.addKeybind(super,         c.XK_1,      wm.desktop, &0))   return;
    // if (wm.addKeybind(super,         c.XK_2,      wm.desktop, &1))   return;
    // if (wm.addKeybind(super,         c.XK_3,      wm.desktop, &2))   return;
    // if (wm.addKeybind(super,         c.XK_4,      wm.desktop, &3))   return;
    // if (wm.addKeybind(super,         c.XK_5,      wm.desktop, &4))   return;
    // if (wm.addKeybind(super,         c.XK_6,      wm.desktop, &5))   return;
    // if (wm.addKeybind(super,         c.XK_7,      wm.desktop, &6))   return;
    // if (wm.addKeybind(super,         c.XK_8,      wm.desktop, &7))   return;
    // if (wm.addKeybind(super,         c.XK_9,      wm.desktop, &8))   return;
    // if (wm.addKeybind(super,         c.XK_0,      wm.desktop, &9))   return;
    // // TODO: Set cursor and root color.
    // // https://gitlab.freedesktop.org/xorg/app/xsetroot/-/blob/master/xsetroot.c might help.
    // wm.execFn(&mem.join(wm.allocator, " ", &.{ "xsetroot -cursor_name", cursor }));
    // if (Bg.path) |bg_img|
    //     wm.execFn(&mem.join(wm.allocator, " ", &.{ "xsetroot -bitmap", bg_img }))
    // else
    //     wm.execFn(&mem.join(wm.allocator, " ", &.{ "xsetroot -bg", Bg.col }));
}
