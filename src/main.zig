pub fn main() u8 {
    if (@import("std").os.argv.len != 1) {
        @import("log.zig").info("Read the README for help.", .{});
        return 1;
    }

    return @import("4096wm.zig").run();
}
