const std  = @import("std");
const mem  = std.mem;
const os   = std.os;
const heap = std.heap;
const log  = @import("log.zig");
const conf = @import("conf.zig");
const c    = @cImport({
    @cInclude("stdlib.h"); // free()
    @cInclude("xcb/xcb.h");
    // @cInclude("xcb/xcb_ewmh.h");
    @cInclude("xcb/xcb_keysyms.h");
    @cInclude("X11/keysym.h");
});

const Self = @This();

const ArgsType = ?*const anyopaque;
const FunType  = ?*const fn (*Self, ArgsType) void;

allocator : mem.Allocator,

conn      : ?*c.xcb_connection_t,
root_win  :   c.xcb_window_t,
focus_win :   c.xcb_window_t,
full_win  :   c.xcb_window_t = 0,
keysyms   : ?*c.xcb_key_symbols_t,

scre_w : u32,
scre_h : u32,

keybinds : std.AutoHashMap(
    struct {
        mod     : u16,
        keycode : c.xcb_keycode_t,
    },
    struct {
        fun  : FunType,
        args : ArgsType,
    }
),
desktops    : [16]std.ArrayList(c.xcb_window_t),
desktop_idx : u4,

pub fn chkCookie(
             self    : *Self,
             cookie  : c.xcb_void_cookie_t,
    comptime err_msg : []const u8,
) bool {
    if (c.xcb_request_check(self.conn, cookie)) |err| {
        log.err(err_msg ++ " (error code: {d})", .{ err.*.error_code });
        c.free(err);
        return true;
    }
    return false;
}

pub fn addKeybind(
    self   : *Self,
    mod    : u16,
    keysym : c.xcb_keysym_t,
    fun    : FunType,
    args   : ArgsType,
) bool {
    const keycode = c.xcb_key_symbols_get_keycode(self.keysyms, keysym).*;
    self.keybinds.put(.{
        .mod     = mod,
        .keycode = keycode,
    }, .{
        .fun  = fun,
        .args = args,
    }) catch return true; // TODO: Error.
    // TODO: Do keys need to be ungrabbed?
    if (self.chkCookie(c.xcb_grab_key_checked(self.conn, 1, self.root_win,
        mod, keycode, c.XCB_GRAB_MODE_ASYNC, c.XCB_GRAB_MODE_ASYNC),
        "Failed to grab key.")) return true;
    return false;
}

pub fn focus(self : *Self, win : c.xcb_window_t) void {
    if (self.chkCookie(c.xcb_set_input_focus_checked(self.conn,
        c.XCB_INPUT_FOCUS_POINTER_ROOT, win, c.XCB_CURRENT_TIME),
        "Failed to set input focus to window.")) return;
    defer self.focus_win = win;
    if (self.focus_win != self.root_win)
        if (self.chkCookie(c.xcb_change_window_attributes_checked(
            self.conn, self.focus_win, c.XCB_CW_BORDER_PIXEL,
            &conf.Win.Border.Unfocus.col),
            "Failed to change border color of window.")) return;
    if (win != self.root_win)
        if (self.chkCookie(c.xcb_change_window_attributes_checked(
            self.conn, win, c.XCB_CW_BORDER_PIXEL,
            &conf.Win.Border.Focus.col),
            "Failed to change border color of window.")) return;
}

pub fn setWinSizeAndPos(
    self : *Self,
    win : c.xcb_window_t,
    x : u32, y : u32,
    w : u32, h : u32,
) void {
    if (self.chkCookie(c.xcb_configure_window_checked(self.conn, win,
        c.XCB_CONFIG_WINDOW_X     | c.XCB_CONFIG_WINDOW_Y      |
        c.XCB_CONFIG_WINDOW_WIDTH | c.XCB_CONFIG_WINDOW_HEIGHT |
        c.XCB_CONFIG_WINDOW_BORDER_WIDTH,
        &[5]u32{ x, y, w, h, conf.Win.Border.size },
    ), "Failed to configure window."))
        _ = self.chkCookie(c.xcb_destroy_window_checked(self.conn, win),
            "Failed to destroy window.");
}

pub fn updateWins(self : *Self, rm_win : ?c.xcb_window_t) void {
    if (rm_win) |_rm_win| {
        if (_rm_win == self.focus_win) self.focus(self.root_win);
        for (self.desktops[self.desktop_idx].items, 0..) |win, idx|
            if (win == _rm_win) {
                _ = self.desktops[self.desktop_idx].orderedRemove(idx);
                _ = self.chkCookie(c.xcb_destroy_window_checked(self.conn, _rm_win),
                    "Failed to destroy window.");
                break;
            };
    }
    switch (conf.mode) {
        .master => {
            const wins = self.desktops[self.desktop_idx].items;
            switch (wins.len) {
                0 => return,
                1 => self.full(&wins[0]),
                else => {
                    for (wins[0..wins.len - 1], 0..) |win, idx| {
                        const h : u32 = @intCast(u32,
                            (self.scre_h - conf.Bar.gap - conf.Win.gap * wins.len) / (wins.len - 1)) - conf.Win.Border.size * 2;
                        self.setWinSizeAndPos(
                            win,
                            conf.Win.gap,
                            conf.Win.gap + conf.Bar.gap + @intCast(u32, idx) * (h + conf.Win.gap + conf.Win.Border.size * 2),
                            self.scre_w / 2 - conf.Win.gap - conf.Win.gap / 2 - conf.Win.Border.size * 2,
                            h,
                        );
                    }
                    self.setWinSizeAndPos(
                        wins[wins.len - 1],
                        self.scre_w / 2 + conf.Win.gap / 2 + conf.Win.Border.size,
                        conf.Win.gap + conf.Bar.gap,
                        self.scre_w / 2 - conf.Win.gap - conf.Win.gap / 2 - conf.Win.Border.size * 2,
                        self.scre_h - conf.Win.gap * 2 - conf.Bar.gap - conf.Win.Border.size * 2,
                    );
                },
            }
        },
        .stack => {}, // TODO
    }
}

pub fn exec(_ : *Self, cmd : ArgsType) void {
    const pid = os.fork() catch |err| {
        log.err("Failed to exec (fork syscall failed). (error: {s})", .{ @errorName(err) });
        return;
    };
    if (pid == 0) {
        // _ = os.linux.syscall0(.setsid); // TODO: Get errno.
        // const err = os.execveZ(
        //     @ptrCast([*:0]const u8, "/bin/sh"),
        //     @ptrCast([*:null]const ?[*:0]const u8, &[_:null]?[*:0]const u8 { "sh", "-c", cmd, null }),
        //     @ptrCast([*:null]const ?[*:0]const u8, &os.environ),
        // );
        // log.err("Failed to exec (execve syscall failed). (error: {s})", .{ @errorName(err) });
        const unistd = comptime @cImport(@cInclude("unistd.h"));
        _ = unistd.setsid();
        _ = unistd.execvp("sh",
            @ptrCast([*c]const [*c]u8, &[_:null]?[*:0]const u8{
                "sh", "-c", @ptrCast(?[*:0]const u8, cmd), null }));
        os.exit(1);
        unreachable;
    }
}

pub fn kill(self : *Self, _ : ArgsType) void {
    log.info("{any}     {?}", .{ self.desktops[self.desktop_idx].items, self.focus_win });
    if (self.focus_win != self.root_win)
        _ = self.chkCookie(c.xcb_kill_client_checked(
            self.conn, self.focus_win), "Failed to kill client.");
}

pub fn full(self : *Self, win : ArgsType) void {
    const full_win = @ptrCast(*const c.xcb_window_t, @alignCast(4, win.?)).*;
    if (full_win == self.full_win and
        self.desktops[self.desktop_idx].items.len != 1)
        self.updateWins(null) // Please don't cause recursion...
    else if (full_win != self.root_win) {
        self.full_win = full_win;
        _ = self.chkCookie(c.xcb_configure_window_checked(self.conn, full_win,
            c.XCB_CONFIG_WINDOW_STACK_MODE, &c.XCB_STACK_MODE_ABOVE),
            "Failed to move window above on stack.");
        self.setWinSizeAndPos(
            full_win,
            conf.Win.gap,
            conf.Win.gap + conf.Bar.gap,
            self.scre_w - conf.Win.gap * 2 - conf.Win.Border.size * 2,
            self.scre_h - conf.Win.gap * 2 - conf.Bar.gap - conf.Win.Border.size * 2,
        );
    }
}

pub fn desktop(self : *Self, idx : ArgsType) void {
    for (self.desktops[self.desktop_idx].items) |win|
        _ = if (self.chkCookie(c.xcb_unmap_window_checked(self.conn, win),
            "Failed to unmap window."))
                self.chkCookie(c.xcb_destroy_window_checked(self.conn, win),
                    "Failed to destroy window.");
    self.desktop_idx = @ptrCast(*const u4, idx.?).*;
    for (self.desktops[self.desktop_idx].items) |win|
        _ = if (self.chkCookie(c.xcb_map_window_checked(self.conn, win),
            "Failed to map window."))
                self.chkCookie(c.xcb_destroy_window_checked(self.conn, win),
                    "Failed to destroy window.");
    // updateWins(null); // Just in case.
}

pub fn top(self : *Self, _ : ArgsType) void {
    if (self.focus_win != self.root_win) {
        for (self.desktops[self.desktop_idx].items, 0..) |win, idx|
            if (win == self.focus_win) {
                _ = self.desktops[self.desktop_idx].orderedRemove(idx);
                break;
            };
        self.desktops[self.desktop_idx].append(self.focus_win) catch |err| {
            log.err("Failed to register window. (error: {s})", .{ @errorName(err) });
            _ = self.chkCookie(c.xcb_destroy_window_checked(self.conn, self.focus_win),
                "Failed to destroy window.");
        };
        // _ = self.chkCookie(c.xcb_configure_window_checked(self.conn, self.focus_win,
        //     c.XCB_CONFIG_WINDOW_STACK_MODE, &c.XCB_STACK_MODE_ABOVE),
        //     "Failed to move window above on stack.");
        self.updateWins(null);
    }
}

// xcb_warp_pointer
// pub fn next(self : *Self, _ : ArgsType) void {}

pub fn run() u8 {
    var self : Self = undefined;

    self.conn = c.xcb_connect(null, null);
    defer c.xcb_disconnect(self.conn);
    if (c.xcb_connection_has_error(self.conn) != 0) {
        log.err("Failed to connect to X server.", .{});
        return 1;
    }

    // TODO: https://github.com/baskerville/bspwm/blob/master/src/bspwm.c#L491
    const scre = c.xcb_setup_roots_iterator(
        c.xcb_get_setup(self.conn)).data orelse {
            log.err("Failed to get screen.", .{});
            return 1;
        };

    self.root_win  = scre.*.root;
    self.focus_win = scre.*.root;

    self.scre_w = scre.*.width_in_pixels;
    self.scre_h = scre.*.height_in_pixels;

    if (self.chkCookie(c.xcb_change_window_attributes_checked(
        self.conn, self.root_win, c.XCB_CW_EVENT_MASK,
        &(c.XCB_EVENT_MASK_ENTER_WINDOW
        // | c.XCB_EVENT_MASK_STRUCTURE_NOTIFY
        | c.XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY
        | c.XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT
        | c.XCB_EVENT_MASK_PROPERTY_CHANGE)),
        "Failed to change root window atributes. Maby another WM running?")) return 1;

    self.keysyms = c.xcb_key_symbols_alloc(self.conn);
    defer c.xcb_key_symbols_free(self.keysyms);

    var arena = heap.ArenaAllocator.init(heap.page_allocator);
    defer arena.deinit();
    self.allocator = arena.allocator();

    self.keybinds = @TypeOf(self.keybinds).init(self.allocator);

    conf.parseAndAplyConf(&self);
    if (self.addKeybind(conf.super | conf.shift, c.XK_q,      null, null))    return 1;
    if (self.addKeybind(conf.super,              c.XK_f,      full, &self.focus_win))    return 1;
    if (self.addKeybind(conf.super,              c.XK_t,      top,  null))    return 1;
    if (self.addKeybind(conf.super,              c.XK_q,      kill, null))    return 1;
    if (self.addKeybind(conf.super,              c.XK_Return, exec, "alacritty")) return 1;
    if (self.addKeybind(conf.super,              c.XK_b,      exec, "qutebrowser")) return 1;
    if (self.addKeybind(conf.super,              c.XK_1,      desktop, &@as(u4, 0)))   return 1;
    if (self.addKeybind(conf.super,              c.XK_2,      desktop, &@as(u4, 1)))   return 1;
    if (self.addKeybind(conf.super,              c.XK_3,      desktop, &@as(u4, 2)))   return 1;
    if (self.addKeybind(conf.super,              c.XK_4,      desktop, &@as(u4, 3)))   return 1;
    if (self.addKeybind(conf.super,              c.XK_5,      desktop, &@as(u4, 4)))   return 1;
    if (self.addKeybind(conf.super,              c.XK_6,      desktop, &@as(u4, 5)))   return 1;
    if (self.addKeybind(conf.super,              c.XK_7,      desktop, &@as(u4, 6)))   return 1;
    if (self.addKeybind(conf.super,              c.XK_8,      desktop, &@as(u4, 7)))   return 1;
    if (self.addKeybind(conf.super,              c.XK_9,      desktop, &@as(u4, 8)))   return 1;
    if (self.addKeybind(conf.super,              c.XK_0,      desktop, &@as(u4, 9)))   return 1;
    // TODO: Set cursor and root color.
    // https://gitlab.freedesktop.org/xorg/app/xsetroot/-/blob/master/xsetroot.c might help.
    self.exec(@ptrCast(ArgsType,
        &(mem.join(self.allocator, " ", &.{ "xsetroot -cursor_name", conf.cursor }) catch return 1)));
    if (conf.Bg.path) |bg_img|
        self.exec(@ptrCast(ArgsType,
            &(mem.join(self.allocator, " ", &.{ "xsetroot -bitmap", bg_img }) catch return 1)))
    else
        self.exec(@ptrCast(ArgsType,
            &(mem.join(self.allocator, " ", &.{ "xsetroot -bg", conf.Bg.col }) catch return 1)));

    _ = c.xcb_flush(self.conn);

    // TODO: This could also use the clone function of ArrayList.
    for (self.desktops, 0..) |_, idx|
        self.desktops[idx] = @TypeOf(self.desktops[idx]).init(self.allocator);
    defer for (self.desktops) |_desktop| _desktop.deinit();
    self.desktop_idx = 0;

    log.info("Entering event loop.", .{});
    defer log.info("Exiting event loop.", .{});
    var ev : ?*c.xcb_generic_event_t = undefined;
    while (blk: {
            ev = c.xcb_wait_for_event(self.conn);
            break :blk ev != null;
        }) {
        switch (ev.?.response_type & ~@as(u8, 0x80)) {
            c.XCB_KEY_PRESS => {
                const key_ev = @ptrCast(*c.xcb_key_press_event_t, ev.?).*;
                if (self.keybinds.get(.{
                    .mod     = key_ev.state,
                    .keycode = key_ev.detail,
                })) |x|
                    if (x.fun) |fun|
                        fun(&self, x.args) else break;
            },
            c.XCB_ENTER_NOTIFY =>
                self.focus(@ptrCast(*c.xcb_enter_notify_event_t, ev.?).*.event),
            c.XCB_MAP_REQUEST => {
                const win = @ptrCast(*c.xcb_map_request_event_t, ev.?).*.window;
                if (self.chkCookie(c.xcb_change_window_attributes_checked(
                    self.conn, win, c.XCB_CW_EVENT_MASK,
                    &(c.XCB_EVENT_MASK_ENTER_WINDOW
                    // | c.XCB_EVENT_MASK_STRUCTURE_NOTIFY
                    | c.XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY)),
                    "Failed to change window atributes."))
                        _ = self.chkCookie(c.xcb_destroy_window_checked(self.conn, win),
                            "Failed to destroy window.");
                if (self.chkCookie(c.xcb_map_window_checked(self.conn, win),
                    "Failed to map window."))
                        _ = self.chkCookie(c.xcb_destroy_window_checked(self.conn, win),
                            "Failed to destroy window.");
                self.desktops[self.desktop_idx].append(win) catch |err| {
                    log.err("Failed to register window. (error: {s})", .{ @errorName(err) });
                    _ = self.chkCookie(c.xcb_destroy_window_checked(self.conn, win),
                        "Failed to destroy window.");
                };
                self.updateWins(null);
                self.focus(win);
            },
            c.XCB_UNMAP_NOTIFY =>
                self.updateWins(@ptrCast(*c.xcb_unmap_notify_event_t, ev.?).*.window),
            // TODO: Moving and resizing windows if mode == stack.
            else => {},
        }
        _ = c.xcb_flush(self.conn);
        c.free(ev);
    }

    return 0;
}
