const std = @import("std");

const stdout = std.io.getStdOut().writer();

pub inline fn err(
    comptime fmt  : []const u8,
             args : anytype,
) void {
    _ = stdout.print("[\x1B[1;31mERROR\x1B[0m]:  \x1B[1;67m\"" ++ fmt ++ "\"\x1B[0m\n", args) catch undefined;
}

pub inline fn info(
    comptime fmt  : []const u8,
             args : anytype,
) void {
    _ = stdout.print("[\x1B[1;32mINFO\x1B[0m]:   \x1B[1;67m\"" ++ fmt ++ "\"\x1B[0m\n", args) catch undefined;
}
