# 4096wm

- Terrible window manager

## Installation

- Clone the repository: ```git clone https://gitlab.com/rwx4096/4096wm.git```

- Install the following dependencies:
	- APT: ```zig libxcb1 libxcb1-dev libxcb-keysyms1 libxcb-keysyms1-dev x11proto-core-dev```
	- AUR, DNF: ```zig libxcb xcb-util-keysyms libx11```
	- Nix: ```zig xorg.libxcb xorg.libxcb.dev xorg.xcbutilkeysyms xorg.xcbutilkeysyms.dev xorg.libX11.dev```

- Build the program: ```./build```

<!-- - Create a configuration file named ```4096wm.conf``` in ```$HOME/.config/```. There is an example configuration file in the project repository. -->
- Add ```exec path/to/4096wm/4096wm``` to the end of your ```.xinitrc``` file, which is located at ```$HOME/.xinitrci/``` or ```/etc/X11/xinit/.xinitrc/``` (For more info, run: ```man startx```)
- All logs are written to stdout, but you can redirect them to any file using the ```>``` or ```>>``` shell operators.

## Usage

- To start a X11 session, run ```startx``` (For more info, run: ```man startx```)
